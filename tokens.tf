resource "vault_token" "nomad" {
  count = length(var.control_plane_ips)

  # Put more restrictive policies here.
  
  policies = [
    "root",
  ]

  role_name = vault_token_auth_backend_role.nomad_server.role_name

  no_parent = true
}
