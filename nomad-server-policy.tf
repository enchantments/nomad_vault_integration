data "vault_policy_document" "nomad_server" {
  # Allow creating tokens under "nomad-cluster" token role. The token role name
  # should be updated if "nomad-cluster" is not used.
  rule {
    path = "auth/token/create/nomad-cluster"
    capabilities = ["update"]
  }

  # Allow looking up "nomad-cluster" token role. The token role name should be
  # updated if "nomad-cluster" is not used.
  rule {
    path = "auth/token/roles/nomad-cluster"
    capabilities = ["read"]
  }

  # Allow looking up incoming tokens to validate they have permissions to access
  # the tokens they are requesting. This is only required if
  # `allow_unauthenticated` is set to false.
  rule {
    path =  "auth/token/lookup"
    capabilities = ["update"]
  }

  # Allow revoking tokens that should no longer exist. This allows revoking
  # tokens for dead tasks.
  rule {
    path =  "auth/token/revoke-accessor"
    capabilities = ["update"]
  }
}

resource "vault_policy" "nomad_server" {
  name   = "nomad-server"
  policy = data.vault_policy_document.nomad_server.hcl
}
