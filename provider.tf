provider "vault" {
  address      = var.vault_address
  token        = var.token
  ca_cert_file = var.ca_file
  client_auth {
    key_file   = var.key_file
    cert_file  = var.cert_file
  }
}
