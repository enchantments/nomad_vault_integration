resource "null_resource" "introduction" {
  count = length(var.control_plane_ips)

  connection {
    type        = "ssh"
    host        = var.control_plane_ips[count.index]
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "file" {
    destination = "/tmp/nomad-vault-token.env"
    content     = "VAULT_TOKEN=${vault_token.nomad[count.index].client_token}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /var/run/nomad",
      "sudo chmod 0400 /tmp/nomad-vault-token.env",
      "sudo chown root:root /tmp/nomad-vault-token.env",
      "sudo mv -f /tmp/nomad-vault-token.env /var/run/nomad/vault-token.env"
    ]
  }
}
