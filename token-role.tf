resource "vault_token_auth_backend_role" "nomad_server" {
  role_name              = "nomad-cluster"
  disallowed_policies    = [vault_policy.nomad_server.name]
  renewable              = true
  orphan                 = true
  token_period           = 259200
  token_explicit_max_ttl = 0
}
